FROM registry.gitlab.com/ugleiton/octant-docker:base

ENV k8sversion=v1.11.5

USER root

# Set the Kubernetes version as found in the UCP Dashboard or API

RUN mkdir /home/user && chown -R user /home/user

# Get and install the kubectl binary.
RUN apt-get update \
 && apt-get install -y curl \
 && curl -LO https://storage.googleapis.com/kubernetes-release/release/$k8sversion/bin/linux/amd64/kubectl \
 && chmod +x ./kubectl \
 && mv ./kubectl /usr/local/bin/kubectl \
 && rm -rf /var/lib/apt/lists/*
#apt install xdg-utils \

USER user

# Run Supervisor
ENTRYPOINT [ "./octant","--disable-open-browser" ]